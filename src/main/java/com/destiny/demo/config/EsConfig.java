//package com.destiny.demo.config;
//
//import org.elasticsearch.client.transport.TransportClient;
//import org.elasticsearch.common.settings.Settings;
//import org.elasticsearch.common.transport.InetSocketTransportAddress;
//import org.elasticsearch.transport.client.PreBuiltTransportClient;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//
///**
// * Created by xieyu on 2018/7/11.
// */
//@Configuration
//public class EsConfig {
//
//    private Logger logger = LoggerFactory.getLogger(EsConfig.class);
//
//    @Bean
//    public TransportClient client() throws UnknownHostException {
//
//        InetSocketTransportAddress node = new InetSocketTransportAddress(
//                InetAddress.getByName("localhost"), 9300
//
//        );
//
//        Settings settings = Settings.builder().put("cluster.name", "destiny");
//
//        TransportClient client = new PreBuiltTransportClient(settings);
//
//        client.addTransportAddress(node);
//
//    }
//
//
//}