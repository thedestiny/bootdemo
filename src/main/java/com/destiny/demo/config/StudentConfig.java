package com.destiny.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Created by xieyu on 2018/6/28.
 */
@Configuration
@ConfigurationProperties( prefix = "student")
@Component
@Data
public class StudentConfig {

    private String name;
    private Integer age;
    private String uuid;
    private String max;
    private String value;
    private String greeting;
    private String[] array;
    private List<Map<String, String>> listProp1; //接收prop1里面的属性值
    private List<String> listProp2 ; //接收prop2里面的属性值
    private Map<String, String> mapProps ; //接收prop1里面的属性值


}