package com.destiny.demo.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by xieyu on 2018/7/11.
 */


@Configuration
@ConfigurationProperties(prefix = "demo")
@PropertySource(value = "classpath:demo.properties")
@Component
@Data
@Slf4j
public class DemoConfig {

    private String name;
    private String data;


}