package com.destiny.demo.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

/**
 * Created by xieyu on 2018/6/28.
 */

@Slf4j
@Component
public class EmailUtils {

    @Autowired
    private JavaMailSenderImpl mailSender;


    public void sendTxtMail(String receiver, String from, String subject, String content) {

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        // 设置收件人，寄件人
        simpleMailMessage.setTo(new String[]{receiver});
        simpleMailMessage.setFrom(from);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(content);
        // 发送邮件
        mailSender.send(simpleMailMessage);

        System.out.println("邮件已发送");

    }


}