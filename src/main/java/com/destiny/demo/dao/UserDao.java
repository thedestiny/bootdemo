package com.destiny.demo.dao;

import com.destiny.demo.domain.po.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by xieyu on 2018/6/28.
 */

@Repository
@Slf4j
public class UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public int add(User user) {

        log.info("prepare add entity -> " + user);

        return jdbcTemplate.update("INSERT INTO `user` (`username`, `address`, `age`, `brithday`, `flag`) VALUES ( ?, ?, ?, ?, ?)",
                user.getUsername(), user.getAddress(), user.getAge(), user.getBirthday(), user.getFlag());

    }


}