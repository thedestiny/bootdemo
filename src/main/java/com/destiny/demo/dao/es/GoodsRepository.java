package com.destiny.demo.dao.es;

import com.destiny.demo.domain.es.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * Created by xieyu on 2018/7/11.
 */

@Component
public interface GoodsRepository extends ElasticsearchRepository<Goods, Long> {

}