package com.destiny.demo.controller;

import com.destiny.demo.config.DemoConfig;
import com.destiny.demo.config.StudentConfig;
import com.destiny.demo.dao.es.GoodsRepository;
import com.destiny.demo.domain.es.Goods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

/**
 * Created by xieyu on 2018/6/28.
 */
@RestController
public class HomeController {

    private Logger logger = LoggerFactory.getLogger(HomeController.class);

    //每页数量
    private Integer PAGESIZE=10;

    @Autowired
    private StudentConfig student;

    @Autowired
    private DemoConfig demo;


    /* 测试加载配置文件 */
    @GetMapping(value = "data")
    public String data() {
        logger.info(" demo is -> " + demo);
        logger.info(" student is -> " + student);
        return student.toString();

    }

    @GetMapping("/")
    public Map<String, Object> greeting() {
        return Collections.singletonMap("message", "Hello World");
    }




}