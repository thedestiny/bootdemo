package com.destiny.demo.controller;

import com.destiny.demo.dao.es.GoodsRepository;
import com.destiny.demo.domain.es.Goods;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Created by xieyu on 2018/6/28.
 */
@RestController
public class GoodsController {

    private Logger logger = LoggerFactory.getLogger(GoodsController.class);

    //每页数量
    private Integer PAGESIZE = 10;


    @Autowired
    private GoodsRepository goodsRepository;


    /* 保存信息 */
    @PostMapping(value = "/es/goods")
    public String saveGoodsInfo(Goods goods) {

        goodsRepository.save(goods);
        return "success";
    }

    /* 查询 http://127.0.0.1:8787/es/goods/15  */
    @GetMapping(value = "/es/goods/{id}")
    public String query(@PathVariable(value = "id") Long id) {


        Optional<Goods> goods = goodsRepository.findById(id);
        Goods temp = goods.get();
        logger.info(" temp -> " + temp);

        return "success";
    }

    // 更新  http://127.0.0.1:8787/es/update?id=15&name=dddddd&description=dddddd放学
    @GetMapping("/es/update")
    public String update(long id, String name, String description) {
        Goods goodsInfo = new Goods();
        goodsInfo.setId(id);
        goodsInfo.setName(name);
        goodsInfo.setDescription(description);

        goodsRepository.save(goodsInfo);
        return "success";
    }

    // 查询  http://127.0.0.1:8787/es/goods/list?pageNumber=0&query=dd
    @GetMapping("/es/goods/list")
    public List<Goods> getList(Integer pageNumber, String query) {
        if (pageNumber == null) pageNumber = 0;
        //es搜索默认第一页页码是0
        SearchQuery searchQuery = getEntitySearchQuery(pageNumber, PAGESIZE, query);
        Page<Goods> goodsPage = goodsRepository.search(searchQuery);
        long num = goodsPage.getTotalElements();

        logger.info(" count -> " + num);

        return goodsPage.getContent();
    }

    private SearchQuery getEntitySearchQuery(int pageNumber, int pageSize, String searchContent) {

        // 构建builder
        BoolQueryBuilder builder = QueryBuilders.boolQuery();
        //builder下有must、should以及mustNot 相当于sql中的and、or以及not

        //设置模糊搜索
        builder.must(QueryBuilders.fuzzyQuery("name", searchContent));
        //设置性别必须为man
        builder.must(new QueryStringQueryBuilder("123").field("description"));
        //按照年龄从高到低
        FieldSortBuilder sort = SortBuilders.fieldSort("id").order(SortOrder.DESC);
        PageRequest page = PageRequest.of(pageNumber, pageSize);
        //构建查询
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();

        queryBuilder.withQuery(builder);
        queryBuilder.withPageable(page);
        queryBuilder.withSort(sort);

        return queryBuilder.build();
    }


}