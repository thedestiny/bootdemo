package com.destiny.demo.domain.po;

import lombok.Data;

import java.util.Date;

/**
 * Created by xieyu on 2018/6/28.
 */

@Data
public class User {

    private Integer id;
    private String username;
    private String address;
    private Integer age;
    private Date birthday;
    private String flag;


    public static void main(String[] args) {

        User user = new User();
        user.setAge(12);

        System.out.println(user.toString());


    }

}