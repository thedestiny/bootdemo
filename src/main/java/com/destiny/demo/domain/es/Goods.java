package com.destiny.demo.domain.es;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

/**
 * Created by xieyu on 2018/7/11.
 */

@Document(
        indexName = "destiny",
        type = "goods"
)
@Data
public class Goods implements Serializable {
    private static final long serialVersionUID = -45657603892390954L;

     private Long id;
     private String name;
     private String description;





}