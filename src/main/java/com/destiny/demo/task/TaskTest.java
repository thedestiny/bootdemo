package com.destiny.demo.task;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by xieyu on 2018/6/28.
 */
@Component
@Slf4j
public class TaskTest {

    private Logger logger = LoggerFactory.getLogger(TaskTest.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    // @Scheduled(fixedRate = 5000)
    // public void reportCurrentTime() {
    //     log.info("The time is now {}", dateFormat.format(new Date()));
    // }


}