package com.destiny.demo.service;

import com.destiny.demo.dao.UserDao;
import com.destiny.demo.domain.po.User;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by xieyu on 2018/6/28.
 */
@Service
@Slf4j
public class UserService {

    private Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserDao userDao;


    public Integer add(User user) {

        return userDao.add(user);
    }


}