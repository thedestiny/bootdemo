package com.destiny.demo.service;

import com.destiny.demo.DemoApplicationTests;
import com.destiny.demo.domain.po.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UserServiceTest extends DemoApplicationTests {

    @Autowired
    private UserService userService;

    @Test
    public void testAdd() {
        User user = new User();

        user.setAddress("uk");
        user.setBirthday(new Date());

        userService.add(user);


    }


}